from random import randint

player_name= input("What is your name? ")
guess_num = 1
for guess_num in range(5):
    month_num = randint(1,12)
    year_num = randint(1924,2004)

    print("Guess", (guess_num+1) , " : ", player_name, "were you born on", month_num, "/", year_num,"?")

    response = input("yes or no? please enter: ")
    if response == "yes":
        print ("I knew it")
        exit()
    elif response == "no" and guess_num>3:
        print("I have other things to do. good bye")
    else:
        print ("drat let me try again")
